all: rpx-builder.so

CFLAGS = -Wall -DHAVE_STDINT_H -g -std=c99
LIBS = -lz
DFLAGS = -x .gdbrc

rpx-builder.so: *.c *.h
	@echo CC *.c
	@gcc $(CFLAGS) -shared -o $@ $< $(LIBS)

test: FORCE rpx-builder.so
	@gdb -q $(DFLAGS) --args ~/src/binutils-2.28/ld/ld-new -plugin ./rpx-builder.so test/*.o -o test/test.elf

test2: FORCE rpx-builder.so
	@gdb -q $(DFLAGS) --args ~/src/binutils-2.28/ld/ld-new -plugin ./rpx-builder.so --whole-archive test/*.a -o test/test.elf

FORCE: