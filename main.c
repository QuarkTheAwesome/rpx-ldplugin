/*  rpx-ldplugin for binutils 2.28
    Copyright (c) 2018 Ash Logan <quarktheawesome@gmail.com>
    This work is licensed under the MIT license. See file LICENSE for a copy.
*/

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/time.h>
#include <zlib.h>

#include "main.h"
#include "plugin-api.h"
#include "elf_abi.h"
#include "claimed_file.h"
#include "rpx_sects.h"

static ld_plugin_register_claim_file tv_register_claim_file = 0;
static ld_plugin_register_all_symbols_read tv_register_all_symbols_read = 0;
static ld_plugin_register_cleanup tv_register_cleanup = 0;
static ld_plugin_add_symbols tv_add_symbols = 0;
static ld_plugin_get_symbols tv_get_symbols = 0;
static ld_plugin_add_input_file tv_add_input_file = 0;
static ld_plugin_get_input_file tv_get_input_file = 0;
static ld_plugin_get_view tv_get_view = 0;
static ld_plugin_release_input_file tv_release_input_file = 0;
static const char* ld_output_filename = NULL;
static char our_output_filename[64] = "";

#define LOG_PREFIX "rpx: "
#define LOG(...) printf(LOG_PREFIX __VA_ARGS__)

//TODO: big-endian hosts
#define S16(x) __builtin_bswap16(x)
#define S32(x) __builtin_bswap32(x)

/*  The linker will call us for every input file asking if we want it.
    If it contains RPX imports/exports, we'll take it!
*/
static enum ld_plugin_status claim_file_handler(
        const struct ld_plugin_input_file* file,
        int* claimed) {
    *claimed = 0;
    lseek(file->fd, file->offset, SEEK_SET);

    if (strlen(our_output_filename)) {
        LOG("Claim handler called after symbol handler!\n");
        return LDPS_OK;
    }

/*  Read input file's ELF header and check it's actually ELF */
    Elf32_Ehdr* ehdr = malloc(sizeof(Elf32_Ehdr));
    if (!ehdr) return LDPS_ERR;
    read(file->fd, ehdr, sizeof(Elf32_Ehdr));

    if (!IS_ELF(*ehdr)) {
        LOG("%s is not an ELF\n", file->name);
        free(ehdr);
        return LDPS_OK;   
    }

/*  Go through file's section headers looking for special RPX sections */
    Elf32_Shdr* shdr = malloc(sizeof(Elf32_Shdr));
    claimed_file_t* claimed_file = NULL;

    for (int shdr_ndx = 0; shdr_ndx < S16(ehdr->e_shnum); shdr_ndx++) {
        off_t shdr_off = S32(ehdr->e_shoff) + (shdr_ndx * sizeof(Elf32_Shdr));
        lseek(file->fd, file->offset + shdr_off, SEEK_SET);
        read(file->fd, shdr, sizeof(Elf32_Shdr));

    /*  Is this an RPX import section? If yes, claim it */
        if (S32(shdr->sh_type) == SHT_LOUSER + 2) {
            if (!claimed_file) {
                claimed_file = alloc_claimed_file();
                claimed_file->file = *file;
            }
            *claimed = 1;
            LOG("Claimed %s!\n", claimed_file->file.name);
            continue;
        }
    }

    free(shdr);
    free(ehdr);
    return LDPS_OK;
}

/*  Let's play linker!
    Put together an object file with all TODO
*/
static enum ld_plugin_status allsym_handler() {
    //enum ld_plugin_status ret;
    rpx_import_sect import_coreinit = { 0 };

/*  Go through previously claimed files, building a list of output sections
    and the functions in them. */
    if (!first_claimed_file) return LDPS_OK;
    claimed_file_t* cl_file = first_claimed_file;
    
    do {
    /*  Check out https://gcc.gnu.org/wiki/whopr/driver#GET_INPUT_FILE...
        it's pretty clear that it should give an "open file descriptor."
        Well, it doesn't, at least not in binutils 2.28...
        I would write an fd check but binutils 2.28 *also* overwrites the file
        name so I can't make my own fd. so, we're not going to call
        get_input_file at all, at least until this bug gets fixed.

        https://i.write.codethat.sucks/@quarktheawesome/99908126591460785 */

        //ret = tv_get_input_file(cl_file->file.handle, &cl_file->file);
        //if (ret != LDPS_OK) continue;
        cl_file->file.fd = open(cl_file->file.name, O_RDONLY);

    /*  Read in ELF header */
        Elf32_Ehdr* ehdr = malloc(sizeof(Elf32_Ehdr));
        lseek(cl_file->file.fd, cl_file->file.offset, SEEK_SET);
        read(cl_file->file.fd, ehdr, sizeof(Elf32_Ehdr));

    /*  Get the section header's string table for later */
        Elf32_Shdr* shstrtab = malloc(sizeof(Elf32_Shdr));
        off_t shstrtab_off = S32(ehdr->e_shoff)
            + (S16(ehdr->e_shstrndx) * sizeof(Elf32_Shdr));
        lseek(cl_file->file.fd, cl_file->file.offset + shstrtab_off, SEEK_SET);
        read(cl_file->file.fd, shstrtab, sizeof(Elf32_Shdr));

    /*  Find some of the other sections we need */
        Elf32_Shdr* strtab = malloc(sizeof(Elf32_Shdr));
        Elf32_Shdr* symtab = malloc(sizeof(Elf32_Shdr));

        Elf32_Shdr* shdr = malloc(sizeof(Elf32_Shdr));
        for (int shdr_ndx = 0; shdr_ndx < S16(ehdr->e_shnum); shdr_ndx++) {
            off_t shdr_off = S32(ehdr->e_shoff) 
                + (shdr_ndx * sizeof(Elf32_Shdr));
            lseek(cl_file->file.fd, cl_file->file.offset + shdr_off, SEEK_SET);
            read(cl_file->file.fd, shdr, sizeof(Elf32_Shdr));

        /*  Get this section's name (this is kind of a hack hehe) */
            #define SNAME_LEN 64 /*TODO is this too long/short?*/
            char sname[SNAME_LEN];
            off_t sname_off = S32(shstrtab->sh_offset) + S32(shdr->sh_name);
        /*  read in (hopefully) too much */
            lseek(cl_file->file.fd, cl_file->file.offset + sname_off, SEEK_SET);
            read(cl_file->file.fd, sname, SNAME_LEN);
        /*  make that safe */
            sname[SNAME_LEN - 1] = '\0';

            if (strncmp(sname, ELF_STRTAB, SNAME_LEN) == 0) {
                LOG("strtab@%d\n", shdr_ndx);
                memcpy(strtab, shdr, sizeof(Elf32_Shdr));
            } else if (strncmp(sname, ELF_SYMTAB, SNAME_LEN) == 0) {
                LOG("symtab@%d\n", shdr_ndx);
                memcpy(symtab, shdr, sizeof(Elf32_Shdr));
            }
        }
    /*  I should check that we actually found strtab here... but meh, it's in
        the spec. */

    /*  Go through the input file's sections */
        for (int shdr_ndx = 0; shdr_ndx < S16(ehdr->e_shnum); shdr_ndx++) {
            off_t shdr_off = S32(ehdr->e_shoff) 
                + (shdr_ndx * sizeof(Elf32_Shdr));
            lseek(cl_file->file.fd, cl_file->file.offset + shdr_off, SEEK_SET);
            read(cl_file->file.fd, shdr, sizeof(Elf32_Shdr));

        /*  Get section name, same as before */
            #define SNAME_LEN 64 /*TODO is this too long/short?*/
            char sname[SNAME_LEN];
            off_t sname_off = S32(shstrtab->sh_offset) + S32(shdr->sh_name);
            lseek(cl_file->file.fd, cl_file->file.offset + sname_off, SEEK_SET);
            read(cl_file->file.fd, sname, SNAME_LEN);
            sname[SNAME_LEN - 1] = '\0';

        /*  TODO work with any libname */
            if (strcmp(".fimport_coreinit", sname) == 0) {
            /*  construct rpx_import_sect */
                if (!import_coreinit.created) {
                    strncpy(import_coreinit.name, ".fimport_coreinit", 64);
                }

            /*  Go look for symbols inside our section (imports!) 
                I reckon it's more efficient to just read in everything at once
                than trying to go one by one */
                Elf32_Sym* syms = malloc(S32(symtab->sh_size));
                off_t syms_off = S32(symtab->sh_offset);
                lseek(cl_file->file.fd, cl_file->file.offset + syms_off,
                    SEEK_SET);
                read(cl_file->file.fd, syms, S32(symtab->sh_size));

                int num_syms = S32(symtab->sh_size) / S32(symtab->sh_entsize);
                for (int sym_ndx = 0; sym_ndx < num_syms; sym_ndx++) {
                    Elf32_Sym* sym = &syms[sym_ndx];
                /*  We want defined, global function symbols in our section */
                    if (sym->st_shndx != STN_UNDEF
                        && ELF32_ST_TYPE(sym->st_info) == STT_FUNC
                        && ELF32_ST_BIND(sym->st_info) == STB_GLOBAL
                        && S16(sym->st_shndx) == shdr_ndx) {
                        
                        #define SYMNAME_LEN 64
                        char symname[SYMNAME_LEN];
                        off_t symname_off = S32(strtab->sh_offset)
                            + S32(sym->st_name);
                        lseek(cl_file->file.fd, cl_file->file.offset
                            + symname_off, SEEK_SET);
                        read(cl_file->file.fd, symname, SYMNAME_LEN);
                        symname[SYMNAME_LEN - 1] = '\0';

                        LOG("Found %s!\n", symname);
                        rpx_import_sect_add_symbol(&import_coreinit, symname, S32(sym->st_value));
                    }
                }

                free(syms);
            }
        }

        free(ehdr);
        free(shstrtab);
        free(strtab);
        free(shdr);

        tv_release_input_file(cl_file->file.handle);
        cl_file = cl_file->next;
    } while(cl_file);

/*  Okay; we should now have a list of everything we need to import.
    Let's make an ELF! */

/*  Sect 1 is fimport_coreinit or whatever, we'll do that later */
/*  sect 2: Start with the most important bit - shstrtab. */
    Elf32_Shdr* shstrtab = calloc(1, sizeof(Elf32_Shdr));
    shstrtab->sh_type = S32(SHT_STRTAB);
    shstrtab->sh_addralign = S32(1);

#define ADD_TO_STRTAB_X(strtab, string, stringlen) \
    strtab##_mem = realloc(strtab##_mem, strtab##_pos + stringlen); \
    memcpy(strtab##_mem + strtab##_pos, string, stringlen); \
    strtab##_pos += stringlen;
#define ADD_TO_STRTAB(strtab, string) \
    ADD_TO_STRTAB_X(strtab, string, sizeof(string))

    size_t shstrtab_pos = 0;
    char* shstrtab_mem = NULL;
    ADD_TO_STRTAB(shstrtab, "");

    shstrtab->sh_name = S32(shstrtab_pos);
    ADD_TO_STRTAB(shstrtab, ELF_SHSTRTAB);

/*  sect 3: Next up, your average strtab */
    Elf32_Shdr* strtab = calloc(1, sizeof(Elf32_Shdr));
    strtab->sh_type = S32(SHT_STRTAB);
    strtab->sh_addralign = S32(1);

    strtab->sh_name = S32(shstrtab_pos);
    ADD_TO_STRTAB(shstrtab, ELF_STRTAB);

    size_t strtab_pos = 0;
    char* strtab_mem = NULL;
    ADD_TO_STRTAB(strtab, "");

/*  sect 4: Symtab! */
    Elf32_Shdr* symtab = calloc(1, sizeof(Elf32_Shdr));
    symtab->sh_type = S32(SHT_SYMTAB);
    symtab->sh_entsize = S32(sizeof(Elf32_Sym));
    symtab->sh_link = S32(3); //strtab
    //TODO if we get problems, try sh_link etc.

    symtab->sh_name = S32(shstrtab_pos);
    ADD_TO_STRTAB(shstrtab, ELF_SYMTAB);

    int num_local_symbols = 0
        + 1 /* fimport_coreinit TODO */
        + 1 /* null symbol */;
    int num_symbols = num_local_symbols
        + import_coreinit.num_symbols;

    symtab->sh_size = S32(num_symbols * sizeof(Elf32_Sym));
    Elf32_Sym* symtab_mem = calloc(1, S32(symtab->sh_size));
    
/*  symbol: fimport_coreinit (sect 1)
    TODO: arrayify this */
    symtab_mem[1].st_info = ELF32_ST_INFO(STB_LOCAL, STT_SECTION);
    symtab_mem[1].st_shndx = S16(1);
    symtab_mem[1].st_name = S32(strtab_pos);
    ADD_TO_STRTAB_X(strtab, import_coreinit.name, strlen(import_coreinit.name) + 1);

/*  fill up the rest from fimport_coreinit
    TODO carry over original meta */
    import_coreinit.crc = crc32(0, Z_NULL, 0);
    for (int i = 2; i < num_symbols; i++) {
        symtab_mem[i].st_info = ELF32_ST_INFO(STB_GLOBAL, STT_FUNC);
        symtab_mem[i].st_value = S32((i - 1) * 8); //TODO comment this
        symtab_mem[i].st_shndx = S16(1);
        symtab_mem[i].st_name = S32(strtab_pos);
        char* name = import_coreinit.symbol_names[i - 2];
        ADD_TO_STRTAB_X(strtab, name, strlen(name) + 1);
        import_coreinit.crc = crc32(import_coreinit.crc, (Bytef*)name, strlen(name) + 1);
    }
    const Bytef* emptybuf = calloc(1, 0x10);
    import_coreinit.crc = crc32(import_coreinit.crc, emptybuf, 0xE);

    off_t biggest_symbol = S32(symtab_mem[num_symbols - 1].st_value);
    symtab->sh_info = S32(num_local_symbols);

/*  Now, assemble coreinit section */
    Elf32_Shdr* coreinit_shdr = calloc(1, sizeof(Elf32_Shdr));
    coreinit_shdr->sh_type = S32(SHT_LOUSER + 2);
    coreinit_shdr->sh_flags = S32(SHF_ALLOC);
    coreinit_shdr->sh_size = S32(biggest_symbol + 8);
    coreinit_shdr->sh_addralign = S32(1);
    coreinit_shdr->sh_name = S32(shstrtab_pos);
    ADD_TO_STRTAB_X(shstrtab, import_coreinit.name, strlen(import_coreinit.name) + 1);

    Elf32_RpxImport* coreinit_mem = calloc(1, S32(coreinit_shdr->sh_size));
    coreinit_mem->ri_count = S32(import_coreinit.num_symbols);
    coreinit_mem->ri_crc = S32(import_coreinit.crc);
    strncpy(coreinit_mem->ri_modname, import_coreinit.name + sizeof(".fimport_") - 1,
        S32(coreinit_shdr->sh_size) - RPXIMPORT_MODNAME_OFFSET);

/*  Some extra sections for later: crcs, file info */
    Elf32_Shdr* rpxinfo_shdr = calloc(1, sizeof(Elf32_Shdr));
    rpxinfo_shdr->sh_type = S32(SHT_LOUSER + 4);
    rpxinfo_shdr->sh_size = S32(0xfb);
    rpxinfo_shdr->sh_addralign = S32(1);

/*  Finally, assemble an ELF header */
    Elf32_Ehdr* ehdr = calloc(1, sizeof(Elf32_Ehdr));
    ehdr->e_ident[EI_MAG0] = ELFMAG0;
    ehdr->e_ident[EI_MAG1] = ELFMAG1;
    ehdr->e_ident[EI_MAG2] = ELFMAG2;
    ehdr->e_ident[EI_MAG3] = ELFMAG3;
    ehdr->e_ident[EI_CLASS] = ELFCLASS32;
    ehdr->e_ident[EI_DATA] = ELFDATA2MSB;
    ehdr->e_ident[EI_VERSION] = EV_CURRENT;
    ehdr->e_type = S16(ET_REL);
    ehdr->e_machine = S16(EM_PPC);
    ehdr->e_version = S32(EV_CURRENT);
    ehdr->e_shoff = S32(sizeof(Elf32_Ehdr));
    ehdr->e_ehsize = S16(sizeof(Elf32_Ehdr));
    ehdr->e_shentsize = S16(sizeof(Elf32_Shdr));
    ehdr->e_shnum = S16(6);
    ehdr->e_shstrndx = S16(2);

/*  Calculate and fix some offsets */
    shstrtab->sh_offset = S32(sizeof(Elf32_Ehdr) + sizeof(Elf32_Shdr) * S16(ehdr->e_shnum));
    shstrtab->sh_size = S32(shstrtab_pos);
    strtab->sh_offset = S32(S32(shstrtab->sh_offset) + shstrtab_pos);
    strtab->sh_size = S32(strtab_pos);
    symtab->sh_offset = S32(S32(strtab->sh_offset) + strtab_pos);
    coreinit_shdr->sh_offset = S32(S32(symtab->sh_offset) + S32(symtab->sh_size));
    rpxinfo_shdr->sh_offset = S32(S32(coreinit_shdr->sh_offset) + S32(coreinit_shdr->sh_size));

/*  Write it out! TODO make this portable */
    snprintf(our_output_filename, 64, "/tmp/rpx-tmp-%08x.o", rand());
    int fd = open(our_output_filename, O_WRONLY | O_CREAT | O_SYNC, 0600);
    write(fd, ehdr, sizeof(Elf32_Ehdr));

/*  Seek past NULL entry and write section headers */
    lseek(fd, S32(ehdr->e_shoff) + sizeof(Elf32_Shdr), SEEK_SET);
    write(fd, coreinit_shdr, sizeof(Elf32_Shdr));
    write(fd, shstrtab, sizeof(Elf32_Shdr));
    write(fd, strtab, sizeof(Elf32_Shdr));
    write(fd, symtab, sizeof(Elf32_Shdr));
    write(fd, rpxinfo_shdr, sizeof(Elf32_Shdr));

/*  Write section contents */
    lseek(fd, S32(shstrtab->sh_offset), SEEK_SET);
    write(fd, shstrtab_mem, S32(shstrtab->sh_size));
    lseek(fd, S32(strtab->sh_offset), SEEK_SET);
    write(fd, strtab_mem, S32(strtab->sh_size));
    lseek(fd, S32(symtab->sh_offset), SEEK_SET);
    write(fd, symtab_mem, S32(symtab->sh_size));
    lseek(fd, S32(coreinit_shdr->sh_offset), SEEK_SET);
    write(fd, coreinit_mem, S32(coreinit_shdr->sh_size));
/*  Spoof rpxinfo */
    lseek(fd, S32(rpxinfo_shdr->sh_offset) + S32(rpxinfo_shdr->sh_size), SEEK_SET);
    write(fd, emptybuf, 1);

    close(fd);

/*  Give our finished file to the linker for processing */
    LOG("Adding %s\n", our_output_filename);
    tv_add_input_file(our_output_filename);

    return LDPS_OK;
}

static enum ld_plugin_status cleanup_handler() {
    if (ld_output_filename) {
        LOG("not reopening %s\n", ld_output_filename);
        //int fd = open(ld_output_filename, O_RDONLY);

        //Elf32_Ehdr ehdr = 
    }
    free_claimed_files();
    return LDPS_OK;
}

enum ld_plugin_status onload(
        struct ld_plugin_tv* tv) {
    if (!tv) return LDPS_ERR;

    LOG("Hi!\n");

    struct timeval tl;
    gettimeofday(&tl, NULL);
    srand(tl.tv_usec + tl.tv_sec);

/*  parse ld_plugin_tv array */
    #define SETVAR(x) x = tv->tv_u.x
    do {
        switch (tv->tv_tag) {
            case LDPT_REGISTER_CLAIM_FILE_HOOK:
                SETVAR(tv_register_claim_file);
                break;
            case LDPT_REGISTER_ALL_SYMBOLS_READ_HOOK:
                SETVAR(tv_register_all_symbols_read);
                break;
            case LDPT_REGISTER_CLEANUP_HOOK:
                SETVAR(tv_register_cleanup);
                break;
            case LDPT_ADD_SYMBOLS:
                SETVAR(tv_add_symbols);
                break;
            case LDPT_GET_SYMBOLS:
                SETVAR(tv_get_symbols);
                break;
            case LDPT_ADD_INPUT_FILE:
                SETVAR(tv_add_input_file);
                break;
            case LDPT_GET_INPUT_FILE:
                SETVAR(tv_get_input_file);
                break;
            case LDPT_GET_VIEW:
                SETVAR(tv_get_view);
                break;
            case LDPT_RELEASE_INPUT_FILE:
                SETVAR(tv_release_input_file);
                break;
            case LDPT_OUTPUT_NAME:
                ld_output_filename = tv->tv_u.tv_string;
                break;
            default:
                break;
        }
    } while (tv++->tv_tag != LDPT_NULL);
    #undef SETVAR

    if (tv_register_claim_file)
        tv_register_claim_file(claim_file_handler);

    if (tv_register_all_symbols_read)
        tv_register_all_symbols_read(allsym_handler);

    if (tv_register_cleanup)
        tv_register_cleanup(cleanup_handler);

    return LDPS_OK;
}
