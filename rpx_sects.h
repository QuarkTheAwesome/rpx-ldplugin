#ifndef __RPX_SECTS__
#define __RPX_SECTS__

#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include "elf_abi.h"

typedef struct _rpx_import_sect {
    bool created;
    char name[64];
    unsigned int crc;

    char** symbol_names;
    off_t* symbol_vals;
    size_t num_symbols;
} rpx_import_sect;
static void rpx_import_sect_add_symbol(rpx_import_sect* sect, char* symname, off_t symval) {
    sect->num_symbols++;

    sect->symbol_names = realloc(sect->symbol_names, sect->num_symbols * sizeof(*sect->symbol_names));
    sect->symbol_names[sect->num_symbols - 1] = malloc(strlen(symname) + 1);
    strncpy(sect->symbol_names[sect->num_symbols - 1], symname, strlen(symname) + 1);

    sect->symbol_vals = realloc(sect->symbol_vals, sect->num_symbols * sizeof(*sect->symbol_vals));
    sect->symbol_vals[sect->num_symbols - 1] = symval;
}

typedef struct _Elf32_RpxImport {
    Elf32_Word ri_count; /* number of imports */
    Elf32_Word ri_crc; /* crc32 of all symbol names + some other guff */
    char ri_modname[]; /* name of module to import from */
} Elf32_RpxImport;
#define RPXIMPORT_MODNAME_OFFSET 8

#endif //__RPX_SECTS__
