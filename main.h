#ifndef __MAIN_H__
#define __MAIN_H__

#include "plugin-api.h"

enum ld_plugin_status onload(struct ld_plugin_tv* tv);
static enum ld_plugin_status claim_file_handler(const struct ld_plugin_input_file* file, int* claimed);

#endif //__MAIN_H__