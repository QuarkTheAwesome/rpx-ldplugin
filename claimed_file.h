#ifndef __CLAIMED_FILE_H__
#define __CLAIMED_FILE_H__

#include <stdlib.h>
#include "plugin-api.h"

typedef struct _claimed_file {
    struct ld_plugin_input_file file;

    struct _claimed_file* next;
} claimed_file_t;

static claimed_file_t* first_claimed_file = NULL;
static claimed_file_t* last_claimed_file = NULL;
static claimed_file_t* alloc_claimed_file() {
    claimed_file_t* new_file = calloc(1, sizeof(claimed_file_t));
    if (last_claimed_file) {
        last_claimed_file->next = new_file;
        last_claimed_file = new_file;
    } else {
        first_claimed_file = new_file;
        last_claimed_file = new_file;
    }
    return new_file;
}
static void free_claimed_files() {
    if (!first_claimed_file) return;
    claimed_file_t* file = first_claimed_file;
    claimed_file_t* tmp;
    while (file->next) {
        tmp = file->next;
        free(file);
        file = tmp;
    }
}

#endif //__CLAIMED_FILE_H__